<?php
// Report simple running errors
# ini_set('display_errors', 1);
# error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
require_once("lib_db.php");
require_once("lib_auth.php");
require_once("sitepath.php");

# $sitepath = "/bqbl";  # not committed for sandboxing

date_default_timezone_set('America/Los_Angeles');

$nfldbconn = connect_nfldb();
$bqbldbconn = connect_bqbldb();

$CURRENT_YEAR = 2020;
$WEEK_1_THURS_DATE = "2020-09-11";
$DB_UPDATE_INTERVAL = 60;  # seconds
$DB_UPDATE_TOUCH_FILE = "/tmp/nfldb_update_time";
$REG_SEASON_END_WEEK = 14;
$PRO_BOWL_WEEK = 15;


$timeout = $DB_UPDATE_INTERVAL - (time()-databaseModificationTime());
$week = min(17, isset($_GET['week']) ? pg_escape_string($_GET['week']) : currentWeek());
$year = isset($_GET['year']) ? pg_escape_string($_GET['year']) : currentYear();
$league = isset($_GET['league']) ? $_GET['league'] : getLeague();
$mainColor = date('m') == '10' ? "#EE3983" : "#E53935";
$googleRed500 = "#F44336";
$googleGreen500 = "#4CAF50";

function currentYear() {
    global $CURRENT_YEAR;
    return $CURRENT_YEAR;
}

function currentWeek() {
    global $WEEK_1_THURS_DATE;
    $now = time();
    $season_start = strtotime($WEEK_1_THURS_DATE) - 2*60*60*24;  # Tuesday
    $weeks = 1+floor(($now-$season_start)/(60*60*24*7));
    return $weeks;
}

function upcomingWeek() {
    global $WEEK_1_THURS_DATE;
    $now = time();
    $season_start = strtotime($WEEK_1_THURS_DATE) - 4*60*60*24;  # Sunday
    $weeks = 1+floor(($now-$season_start)/(60*60*24*7));
    return $weeks;
}

function currentCompletedWeek() {
    return currentWeek() - 1;
}

function isPast($year, $week) {
  global $CURRENT_YEAR;
  return $year < $CURRENT_YEAR || ($year == $CURRENT_YEAR && $week <= currentCompletedWeek());
}

function weekCutoffTime($week) {
    global $WEEK_1_THURS_DATE;
    $now = time();
    if ($week == 17) {
        $season_first_sunday = strtotime($WEEK_1_THURS_DATE . " 10:00:00") + 24*60*60 * 3;  # Sunday 10:00AM PST
        return $season_first_sunday + 7*24*60*60*($week - 1);
    } else {
        $season_start = strtotime($WEEK_1_THURS_DATE . " 18:30:00");  # Thursday 5:30PM PST
        return $season_start + 7*24*60*60*($week - 1);
    }
}

function isGameFinished($gsis_id) {
    global $nfldbconn;
    $query = "SELECT finished FROM game WHERE gsis_id='$gsis_id';";
    return pg_fetch_result(pg_query($nfldbconn, $query), 0);
}

function getLeague() {
    $year=currentYear();
    if(isset($_SESSION['league'])) {
        return $_SESSION['league'];
    } elseif(isset($_SESSION['user'])) {
        $query = "SELECT league FROM membership JOIN users ON membership.bqbl_team=users.id WHERE username='$_SESSION[user]' AND year='$year';";
        $league = pg_fetch_result(pg_query($GLOBALS['bqbldbconn'], $query),0);
        return ($league != "") ? $league : "nathans";
    } else return "nathans";
}

function getDomain() {
    global $sitename;
    return $sitename;
}

function getTeamCodes($team) {
  $query = "SELECT id, alt_id_1, alt_id_2, alt_id_3 FROM nfl_teams WHERE id='$team' OR alt_id_1='$team' OR alt_id_2='$team' OR alt_id_3='$team';";
  $result = pg_query($GLOBALS["bqbldbconn"], $query);
  list($id, $alt_id_1, $alt_id_2, $alt_id_3) = pg_fetch_array($result);
  $teamList = array();
  $teamList[] = $id;
  if (!empty($alt_id_1)) {
    $teamList[] = $alt_id_1;
  }
  if (!empty($alt_id_2)) {
    $teamList[] = $alt_id_2;
  }
  if (!empty($alt_id_3)) {
    $teamList[] = $alt_id_3;
  }
  return $teamList;
}

function getTeamCodesSQL($team) {
  $list = "(";
  $team_ids = getTeamCodes($team);
  foreach (getTeamCodes($team) as $team_id) {
    $list .= "'$team_id',";
  }
  return substr($list, 0, strlen($list) - 1) . ')';
}

function getPrimaryTeamCode($team) {
	return getTeamCodes($team)[0];
}

function getBqblTeam($user) {
    global $bqbldbconn;
    return pg_fetch_result(pg_query($bqbldbconn, "SELECT id FROM users WHERE username='$user';"), 0);
}

function bqblIdToTeamName($id) {
    global $bqbldbconn;
    return pg_fetch_result(pg_query($bqbldbconn, "SELECT team_name FROM users WHERE id='$id';"), 0);
}

function bqblIdToUserName($id) {
    global $bqbldbconn;
    return pg_fetch_result(pg_query($bqbldbconn, "SELECT username FROM users WHERE id='$id';"), 0);
}

function bqblIdToLeague($id, $year) {
    global $bqbldbconn;
    return pg_fetch_result(pg_query($bqbldbconn, "SELECT league FROM membership WHERE bqbl_team='$id' AND year='$year';"), 0);
}

function getBqblTeamLink($year, $league, $id) {
    global $sitepath;
    $link = "$sitepath/team.php?team=" . bqblIdToUserName($id);
    if ($year != $CURRENT_YEAR) $link .= "&year=$year";
    return $link;
}

function getNflTeamLink($year, $team) {
    global $sitepath;
    $link = "$sitepath/nfl.php?team=$team";
    if ($year != $CURRENT_YEAR) $link .= "&year=$year";
    return $link;
}

function bqblUserImage($id) {
    global $bqbldbconn;
    return pg_fetch_result(pg_query($bqbldbconn, "SELECT image_url FROM users WHERE id='$id';"), 0);
}

function databaseModificationTime() {
    global $DB_UPDATE_TOUCH_FILE;
    $file = $DB_UPDATE_TOUCH_FILE;
    return filemtime($file);
}

function gameTypeById($gsis) {
global $nfldbconn;
    $query = "SELECT start_time
      FROM game
      WHERE gsis_id='$gsis';";
    $result = pg_query($nfldbconn, $query);
    if(pg_num_rows($result) == 0) { // Bye week
        return -1;
    }
    list($gametime) = pg_fetch_array($result,0);
    if(strtotime($gametime) > time()) {
        return 2; // Future game
    }
    return 1; // Current or past game
}

function nflMatchup($year, $week, $team) {
    global $nfldbconn;
		$team_list = getTeamCodesSQL($team);
    $query = "SELECT home_team, away_team
              FROM game
              WHERE (home_team IN $team_list or away_team IN $team_list) AND season_year='$year' 
              AND week='$week' AND season_type='Regular';";
    $result = pg_query($GLOBALS['nfldbconn'], $query);

    if(pg_num_rows($result) == 0) { // Bye week
        return array();
    }
    return list($home_team,$away_team) = pg_fetch_array($result,0);
}

function gameType($year, $week, $team) {
    global $nfldbconn;
		$team_list = getTeamCodesSQL($team);
    $query = "SELECT start_time
              FROM game
              WHERE (home_team IN $team_list or away_team IN $team_list) AND season_year='$year' 
              AND week='$week' AND season_type='Regular';";
    $result = pg_query($GLOBALS['nfldbconn'], $query);
    if(pg_num_rows($result) == 0) { // Bye week
        return -1;
    }
    list($gametime) = pg_fetch_array($result,0);
    if(strtotime($gametime) > time()) {
        return 2; // Future game
    }
    return 1; // Current or past game
}

function gsisId($year, $week, $team) {
  global $nfldbconn;
  $team_list = getTeamCodesSQL($team);
  $query = "SELECT gsis_id
            FROM game
            WHERE (home_team IN $team_list or away_team IN $team_list) AND season_year='$year' 
            AND week='$week' AND season_type='Regular';";
  $result = pg_query($GLOBALS['nfldbconn'], $query);
  if(pg_num_rows($result) == 0) { 
    return null;
  }
  list($gsis_id) = pg_fetch_array($result,0);
  
  return $gsis_id;
}

function gameUrl($gsis_id) {
  global $nfldbconn;
  $query = "SELECT season_year, week, home_team, away_team
            FROM game
            WHERE gsis_id='$gsis_id';";
  $result = pg_query($GLOBALS['nfldbconn'], $query);
  if(pg_num_rows($result) == 0) { 
    return "http://nfl.com";
  }
  list($year, $week, $home_team, $away_team) = pg_fetch_array($result,0);
  $home_team = getPrimaryTeamCode($home_team);
  $away_team = getPrimaryTeamCode($away_team);

  $query = "SELECT name
            FROM team
            WHERE team_id='$away_team';";
  list($away_name) = pg_fetch_array(pg_query($GLOBALS['nfldbconn'], $query), 0);  
  $query = "SELECT name
            FROM team
            WHERE team_id='$home_team';";
  list($home_name) = pg_fetch_array(pg_query($GLOBALS['nfldbconn'], $query), 0);  
  
  return sprintf("https://www.nfl.com/gamecenter/%s/%s/REG%s/%s@%s", $gsis_id, $year, $week, strtolower($home_name), strtolower($away_name));
}

function gameTypeAndTime($gsis_id) {
  global $nfldbconn;
  $query = "SELECT start_time, finished
            FROM game
            WHERE gsis_id='$gsis_id';";
  $result = pg_query($GLOBALS['nfldbconn'], $query);
  if(pg_num_rows($result) == 0) { 
    return array(-1, "BYE"); // Bye week
  }
  list($gametime, $finished) = pg_fetch_array($result,0);
  $start = strtotime($gametime);
  if($start > time()) {
    return array(2, date("D g:i", $start)); // Future game
  }
  if ($finished == "t") {
    return array(3, "FINAL");
  }
  $query = "SELECT time FROM play WHERE gsis_id='$gsis_id' ORDER BY play_id DESC LIMIT 1;";
  $result = pg_query($GLOBALS['nfldbconn'], $query);
  if(pg_num_rows($result) == 0) { // Bye week
    return array(1, "15:00 Q1"); // About to start
  }
  list($time) = pg_fetch_array($result,0);
  $time = str_replace('(', '', str_replace(')', '', $time));
  $quarter_and_time = explode(',', $time);
  $quarter = $quarter_and_time[0];
  $qTime = 15 * 60 - intval($quarter_and_time[1]);
  $minutes = $qTime / 60;
  $seconds = $qTime % 60;
  if ($quarter == "Final") return array(3, "FINAL");
  if ($quarter == 2 && $qTime == 0) return array(1, "HALF");
  if ($quarter == "Half") return array(1, "HALF");
  return array(1, sprintf("%d:%02d %s",  $minutes, $seconds, $quarter)); // Current game
}

function nflTeams() {
    global $bqbldbconn;
    $teams = array();
    $query = "SELECT id from nfl_teams;";
    $result = pg_query($bqbldbconn, $query);
    while(list($team) = pg_fetch_array($result)) {
        $teams[] = $team;
    }
    return $teams;
}

function nflIdToCityTeamName($id) {
    global $bqbldbconn;
    $query = "SELECT city, name FROM nfl_teams WHERE id='$id';";
    $result = pg_query($bqbldbconn, $query);
    return pg_fetch_array($result);
}

function bqblTeams($league, $year, $sortByDraftOrder=false) {
    global $bqbldbconn;
    $bqbl_teamname = array();
    $orderClause = $sortByDraftOrder ? "ORDER BY draft_order ASC" : "ORDER BY team_name ASC";
    $checkLeague = ($league == "probowl") ? "" : "AND league = '$league'";
    $query = "SELECT bqbl_team, team_name 
              FROM membership JOIN users ON membership.bqbl_team=users.id 
              WHERE year='$year' $checkLeague $orderClause;";
    $result = pg_query($bqbldbconn, $query);
    while(list($id,$team_name) = pg_fetch_array($result)) {
        $bqbl_teamname[$id] = $team_name;
    }
    return $bqbl_teamname;
}


function getRosters($year, $league, $playoffs=false) {
    global $bqbldbconn;
    $roster = array();
    foreach (nflTeams() as $team) {
        $roster[$team] = array();
    }
    $roster_table = $playoffs ? "playoff_roster" : "roster";
    $checkLeague = ($league == "probowl") ? "" : "AND league = '$league'";
    $query = "SELECT bqbl_team, nfl_team
              FROM $roster_table
              WHERE year = $year $checkLeague;";
    $result = pg_query($bqbldbconn, $query);
    while(list($bqbl_team, $nfl_team) = pg_fetch_array($result)) {
        $roster[$bqbl_team][] = $nfl_team;
    }
    return $roster;
}

function getLineups($year, $week, $league) {
    global $bqbldbconn;
    $lineup = array();
    foreach (bqblTeams($league, $year) as $id => $name) {
        $lineup[$id] = array();
    }
    $checkLeague = ($league == "probowl") ? "" : "AND league = '$league'";
    $query = "SELECT bqbl_team, starter1, starter2
                FROM lineup
                  WHERE year = $year AND week = $week $checkLeague;";
    $result = pg_query($bqbldbconn, $query);
    while(list($bqbl_team,$starter1,$starter2) = pg_fetch_array($result)) {
        $lineup[$bqbl_team][] = $starter1;
        $lineup[$bqbl_team][] = $starter2;
    }
    return $lineup;
}

function getMatchups($year, $week, $league) {
    global $bqbldbconn;
    $matchup = array();
    $query = "SELECT team1, team2
            FROM schedule
              WHERE year = $year AND week = $week AND league='$league';";
    $result = pg_query($bqbldbconn, $query);
    while(list($team1,$team2) = pg_fetch_array($result)) {
        $matchup[$team1] = $team2;
    }
    return $matchup;
}

function getOpponents($year, $league, $team) {
    global $bqbldbconn;
    $opponents = array();
    $query = "SELECT team1, team2, week
              FROM schedule
              WHERE year = $year AND league='$league' AND (team1='$team' OR team2='$team')
              ORDER BY week ASC;";
    $result = pg_query($bqbldbconn, $query);
    while(list($team1,$team2,$week) = pg_fetch_array($result)) {
        if ($team1==$team) {
            $opponents[$week] = $team2;
        } else {
            $opponents[$week] = $team1;
        }
    }
    return $opponents;
}

function bqblTeamStrToInt($bqblTeam) {
    global $bqbldbconn;
    $query = "SELECT id FROM users WHERE username='$bqblTeam';";
    return pg_fetch_result(pg_query($bqbldbconn, $query), 0);
}

function isTed($bqblTeam) {
    global $bqbldbconn;
    $query = "SELECT * FROM users WHERE id='$bqblTeam' AND username LIKE 'eltedador%';";
    return pg_num_rows(pg_query($bqbldbconn, $query)) > 0;
}

function footer() {
    global $bqbldbconn, $nfldbconn;
    pg_close($bqbldbconn);
    pg_close($nfldbconn);
}


function getUrl($league, $year, $week, $autorefresh=false) {
    $url = "$_SERVER[PHP_SELF]?league=$league&year=$year&week=$week";
    if ($autorefresh) {
        $url .= "&autorefresh";
    }
    return $url;
}

function urlWithParameters($url, $parameters) {
    if ($parameters == null || count($parameters) == 0) {
        return url;
    }
    $paramstrings = array();
    foreach ($parameters as $key => $val) {
        $paramstrings[] = "$key=$val";
    }
    return $url . "?" . implode("&", $paramstrings);
}

function ui_header($title="", $showLastUpdated=false, $showAutorefresh=false, $showWeekDropdown=false) {
global $year, $week, $league, $timeout, $sitepath;
$refresh_visibility = $showAutorefresh ? "inherit" : "hidden";
$autorefresh = isset($_GET['autorefresh']);
$autorefresh_url = getUrl($league, $year, $week, !$autorefresh);
if ($autorefresh) {
    if ($timeout < 0 && $timeout > -$DB_UPDATE_INTERVAL) $timeout=0;
    if ($timeout >= 0) {
        $timeout *= 1000;  # millis
        $timeout += rand(15000,20000);  # allow for update + prevent DDOS
        echo "<script type='text/javascript'>
        setTimeout(function() {location.reload();}, $timeout);
        </script>";
    }
}
$refreshicon = $autorefresh ? "av:pause-circle-outline" : "autorenew";

# TODO(sam): When paper-dropdown-menu comes out, replace <select> with it,
# then replace the true ternary with "inherit"
$week_visibility = $showAutorefresh ? "inherit" : "hidden";
$selected_week = $week-1;

$updateTime = date("n/j g:i:s A, T", databaseModificationTime());
$lastUpdatedString = $showLastUpdated ? "Last Updated at $updateTime" : "&nbsp;";

$nav_items = array(
    "BQBL Scoreboard" => "$sitepath/matchup.php",
    "NFL Scoreboard" => "$sitepath/week.php",
    "Standings" => "$sitepath/standings.php",
    "Schedule" => "$sitepath/schedule.php",
    "Weekly Rankings" => "$sitepath/leaderboard.php",
    "Season Rankings" => "$sitepath/seasonleaderboard.php",
    "Set Lineup" => "$sitepath/lineup.php",
    "Input Extra Points" => "$sitepath/extrapoints.php",
    "Rantland" => "/rantland",
    "League Management" => "$sitepath/leaguemanagement.php",
    "Past Champions" => "$sitepath/champions.php",
    "BQBL Cares" => "$sitepath/cares",
    "Logout" => "$sitepath/auth/logout.php"
);

if (!isset($_SESSION['user'])) {
    unset($nav_items['Input Extra Points']);
    unset($nav_items['Logout']);
    unset($nav_items['Set Lineup']);
}

$nav_block = "";
$selected_nav_index = -1;
$nav_index = 0;
foreach ($nav_items as $label => $url) {
    if ($url == $_SERVER['PHP_SELF']) {
        $selected_nav_index = $nav_index;
    }
    $nav_index++;
    $nav_block .= "
        <paper-item style='min-height: 64px;'
          onclick=\"window.location.assign('$url');document.getElementById('week-drawer').closeDrawer();\">
            $label
        </paper-item>";
}

if(isset($_GET['teamnum'])) {
    $authbqblteam = pg_escape_string($_GET['teamnum']);
} elseif(isset($_SESSION['user'])) {
    $authbqblteam= getBqblTeam($_SESSION['user']);
	if(!isset($_SESSION['bqbl_team'])) {
		$_SESSION['bqbl_team'] = $authbqblteam;
	}
}

if ($authbqblteam != null) {
    $teamname = bqblIdToTeamName($authbqblteam);
    $image_url = bqblUserImage($authbqblteam);
    $userheader = "<div id='user-avatar' style='margin:8px 0;background-image:url($image_url);margin-right:8px;'></div><div>$teamname</div>";
} else {
$image_url = "$sitepath/media/avatar_default.jpg";
    $userheader = "<div id='user-avatar' style='background-image:url($image_url);margin-right:8px;'></div><div style='margin-left:3%;'><a href='$sitepath/auth/login.php' style='color:white;font-size:125%;font-weight:400;'>Login</a></div>";

}

$onload = "document.getElementById('drawer-header').style.height=68;
document.getElementById('user-avatar').style.width = document.getElementById('user-avatar').clientHeight;
document.getElementById('week-dropdown-filler').style.paddingLeft = document.getElementById('week-dropdown-menu').clientWidth;";

echo <<<END
<html><head>
<script src="/bower_components/webcomponentsjs/webcomponents.min.js"></script>
<link rel="import" href="/bower_components/polymer/polymer.html">
<link rel="import" href="/bower_components/paper-material/paper-material.html">
<link rel="import" href="/bower_components/iron-icons/av-icons.html">
<link rel="import" href="/bower_components/iron-icons/iron-icons.html">
<link rel="import" href="/bower_components/iron-icon/iron-icon.html">
<link rel="import" href="/bower_components/paper-dropdown-menu/paper-dropdown-menu.html">
<link rel="import" href="/bower_components/paper-styles/paper-styles.html">
<link rel="import" href="/bower_components/paper-input/paper-input.html">
<link rel="import" href="/bower_components/paper-input/paper-input-error.html">
<link rel="import" href="/bower_components/paper-icon-button/paper-icon-button.html">
<link rel="import" href="/bower_components/paper-menu/paper-menu.html">
<link rel="import" href="/bower_components/paper-item/paper-item.html">
<link rel="import" href="/bower_components/paper-button/paper-button.html">
<link rel="import" href="/bower_components/paper-header-panel/paper-header-panel.html">
<link rel="import" href="/bower_components/paper-drawer-panel/paper-drawer-panel.html">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
<link href='$sitepath/lib/common.css' rel='stylesheet' type='text/css'>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />

<title>$title</title>
</head>
<body style='margin:0px;' bgcolor='#F8F8F8' onload="$onload">
<paper-drawer-panel id="week-drawer" force-narrow="false" drawer-width="20%">
    <div drawer> 
    <paper-header-panel class="scrollable-panel">
        <div id="drawer-header" class='paper-header' style="padding-left:0;">
            <div style="display:flex;justify-content:flex-start;align-items:center;height:100%;">
                <paper-icon-button style="padding:4.5mm;" icon="chevron-left" role="button" class="huge x-scope paper-icon-button-0" paper-drawer-toggle>
                </paper-icon-button>
                $userheader
            </div>
        </div>
        <paper-menu selected="$selected_nav_index">
            $nav_block
        </paper-menu>
    </paper-header-panel>
    </div>
    
    <div main>
    <paper-header-panel class="scrollable-panel">
        <div id="main-header" class='paper-header'>
            <div style="display:table-cell;">
                <paper-icon-button style="padding:4.5mm;" icon="menu" role="button" class="huge x-scope paper-icon-button-0" paper-drawer-toggle  >
                </paper-icon-button>
            </div>
            <div id="week-dropdown-filler" class="week-dropdown" style="visibility:hidden;"></div>
            <div class="header-title">
                <h1 style="margin:0px;">$title</h1>
                $lastUpdatedString
            </div>
            <div style="display:table-cell;visibility:$week_visibility">$weekdropdown</div>
            <div style="display:table-cell;visibility:$refresh_visibility;">
                <paper-icon-button style="padding:4.5mm;" icon="$refreshicon" role="button" class="huge x-scope paper-icon-button-0" onClick="window.location.assign('$autorefresh_url');">
                </paper-icon-button>
            </div>
        </div>
        <div id='content' align='center' style="padding-bottom:32px;">
END;
#echo <<<END
#        <marquee>
#            <img src="$sitepath/media/banner.gif" />
#        </marquee>
#END;
}

function ui_footer() {
global $mainColor;
echo <<<END
</div>
</div>
</paper-header-panel>
</div>
</paper-drawer-panel>
</body>
<style is="custom-style">
.paper-header {
    background-color: $mainColor;
}
</style>
END;
}
?>
