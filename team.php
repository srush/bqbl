<?php
require_once "lib/lib.php";
require_once "lib/scoring.php";


if(isset($_GET['team'])) {
    $bqblTeam = bqblTeamStrToInt(pg_escape_string($_GET['team']));
} elseif(isset($_GET['teamnum'])) {
    $bqblTeam = pg_escape_string($_GET['teamnum']);
    } elseif(isset($_SESSION['user'])) {
    $bqblTeam = getBqblTeam($_SESSION['user']);
    if(!isset($_SESSION['bqbl_team'])) {
        $_SESSION['bqbl_team'] = $bqblTeam;
    }
} else {
    echo "Please set the 'team' parameter!";
    exit(0);
}

$league = bqblIdToLeague($bqblTeam, $year);
$week = MIN($week, $REG_SEASON_END_WEEK);
ui_header($title=bqblIdToTeamName($bqblTeam));

echo '<paper-material elevation="2">';

$rosters = getRosters($year, $league, $false /* not playoff */);
$roster = $rosters[$bqblTeam];
$opponents = getOpponents($year, $league, $bqblTeam);
$games = array();
for ($i = 1; $i <= $week; $i++) {
    foreach ($roster as $nflTeam) {
        $games[] = array($year, $i, $nflTeam);
    }
    foreach ($rosters[$opponents[$i]] as $nflTeam) {
        $games[] = array($year, $i, $nflTeam);
    }    
}
$gamePoints = getPointsBatch($games);

echo '<div class="table">';
echo "<div class=\"header row\"><div class=\"cell\"></div>";
foreach ($roster as $nflTeam) {
  echo "<div class=\"cell\"><a class='nolinkcolor' href='" . getNflTeamLink($nflTeam, $year) . "'>$nflTeam</a></div>\n";
}


echo "<div class=\"cell\">Ideal Score</div>
<div class=\"cell\">Score</div>
<div class=\"cell\">Opponent</div>
<div class=\"cell\">Opp. Score</div>
<div class=\"cell\">Opp. Ideal Score</div>
</div>";

for ($i = 1; $i <= $week; $i++) {
$lineups = getLineups($year, $i, $league);
echo "<div class=\"row\"><div class=\"cell\"><a class='nolinkcolor' href='$sitepath/matchup.php?year=$year&week=$i&league=$league'>Week $i</a></div>";
    foreach ($roster as $nflTeam) {
        $started = ($lineups[$bqblTeam][0] == $nflTeam) || ($lineups[$bqblTeam][1] == $nflTeam);
        $color = $started ? 'style="background: #CCCCCC;"' : "";
        echo "<div class=\"cell\" $color>";
        $totalPoints = totalPoints($gamePoints[$year][$i][$nflTeam]);
        echo $totalPoints;
        echo "</div>";
    }

    $opponent = $opponents[$i];                                                                       
    $total = totalTeamScore($gamePoints[$year][$i], $roster, $lineups[$bqblTeam]); 
    $ideal = idealTeamScore($gamePoints[$year][$i], $roster, $lineups[$bqblTeam]);
    $oppTotal = totalTeamScore($gamePoints[$year][$i], $rosters[$opponent], $lineups[$opponent]);
    $oppIdeal = idealTeamScore($gamePoints[$year][$i], $rosters[$opponent], $lineups[$opponent]);
    $background = ($total <= $oppTotal && $ideal > $oppTotal) ? "background:$googleGreen500;" : "";

    echo "<div class=\"cell\" style=\"$background\">$ideal</div>";
    echo "<div class=\"cell\">$total</div>";
    
    if ($oppTotal < $total) {
        $color = $googleGreen500;
    } elseif ($oppTotal > $total) {
        $color = $googleRed500;
    } else {
        $color = "#CCCCCC";
    }
    echo "<div class=\"cell\" style=\"background:$color;\"><a class='nolinkcolor' href='" . getBqblTeamLink($year, $league, $opponent) . "'>" . bqblIdToTeamName($opponent) . "</a></div>";
    echo "<div class=\"cell\">$oppTotal</div>";

    $background = ($oppTotal <= $total && $oppIdeal > $total) ? "background:$googleRed500;" : "";
    echo "<div class=\"cell\" style=\"$background\">$oppIdeal</div>";
    echo "</div>\n";
}

echo "</paper-material>";
?>

<style is="custom-style">
paper-material {
    display: inline-block;
    background-color: #FFFFFF;
    padding: 32px;
    margin: 32px 32px 0 32px;
}

.nflteam paper-material {
    display: inline-block;
    background-color: #FFFFFF;
    padding: 8px;
    margin: 12px;
}

.loss {
    background-color: var(--paper-red-500);
}

.win {
    background-color: var(--paper-green-500);
}

.row {
    display: table-row;
}

.cell {
    display: table-cell;
}

.table {
  display: table;
  border-collapse: separate;
  font-size: 1vw;
  text-align: center;
}

.table .cell {
  border-top: 1px solid #e5e5e5;
  padding: 8px;
}

.table .thickline .cell {
  border-bottom: 5px solid #000000;
}

.table .header .cell {
    border-top: 0;
    font-weight: bold;
    font-size: 110%;
    padding-top: 0;
}

.cardheader {
    display:inline-block;
    font-weight: bold;
    font-size: 150%;
    padding-bottom: 16px;
}
</style>

<?php
ui_footer();
?>
