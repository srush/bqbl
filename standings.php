<?php
require_once "lib/lib.php";
require_once "lib/scoring.php";

$completed_week = currentCompletedWeek();
$year = isset($_GET['year']) ? pg_escape_string($_GET['year']) : currentYear();
$week = min(15, isset($_GET['week']) && (pg_escape_string($_GET['week']) <= $completed_week + 1 || $year < currentYear())
        ? pg_escape_string($_GET['week']) : currentCompletedWeek());
$week = min($week, $REG_SEASON_END_WEEK);
$league = isset($_GET['league']) ? $_GET['league'] : getLeague();

ui_header("$year Standings");

$bqbl_teamname = bqblTeams($league, $year);

echo '<paper-material elevation="2">';
echo '<div id="standings-table">';
echo "<div class='header row' style='display:table-row;'>
<div class='cell' style='display:table-cell;'></div>
<div class='cell' style='display:table-cell;'>Team</div>
<div class='cell' style='display:table-cell;'>Wins</div>
<div class='cell' style='display:table-cell;'>Losses</div>
<div class='cell' style='display:table-cell;'>Points For</div>
<div class='cell' style='display:table-cell;'>Points Against</div>
<div class='cell' style='display:table-cell;'>Point Differential</div>
<div class='cell' style='display:table-cell;'>Streak</div>
<div class='cell' style='display:table-cell;'>Coaching Losses</div>
<div class='cell' style='display:table-cell;'>Opp. Coaching Losses</div>
</div>";
$rank = 0;

$query = "
  SELECT
    bqbl_team,
    wins,
    losses,
    coaching_losses,
    opponent_coaching_losses,
    points_for,
    points_against,
    point_differential,
    streak_length,
    CASE 
      WHEN streak_type = 1 THEN 'W'
      WHEN streak_type = 0 THEN 'L'
      ELSE 'T'
    END AS streak_type_string
    FROM standings($year, $week, '$league');";
$result = pg_query($GLOBALS['bqbldbconn'],$query);
while(list(
    $bqbl_team,
    $wins,
    $losses,
    $coaching_losses,
    $opponent_coaching_losses,
    $points_for,
    $points_against,
    $point_differential,
    $streak_length,
    $streak_type) = pg_fetch_array($result)) {
  $rank++;
  $thickline = ($rank==4) ? "thickline" : "";

  switch ($bqbl_team) {
      case 1:
          $color = "samdwich";
          break;
      case 2:
          $color = "sworls";
          break;
      case 3:
          $color = "jhka3";
          break;
      case 4:
          $color = "murphmanjr";
          break;
      case 5:
          $color = "lukabear";
          break;
      case 6:
          $color = "anirbaijan";
          break;
      case 7:
          $color = "kvk";
          break;
      case 8:
          $color = "palc";
          break;
      default:
          $color = "";
  }

  echo "<div class='row $thickline' style='display:table-row;'>
  <div class='cell' style='display:table-cell;'>$rank.</div>
  <div class='cell $color' style='display:table-cell;' $color><a class='nolinkcolor' href='" . getBqblTeamLink($year, $league, $bqbl_team) . "'>$bqbl_teamname[$bqbl_team]</a></div>
  <div class='cell' style='display:table-cell;'>$wins</div>
  <div class='cell' style='display:table-cell;'>$losses</div>
  <div class='cell' style='display:table-cell;'>$points_for</div>
  <div class='cell' style='display:table-cell;'>$points_against</div>
  <div class='cell' style='display:table-cell;'>";
  if ($point_differential >= 0) {
      echo "+";
  }
  echo "$point_differential</div>";
  echo "<div class='cell' style='display:table-cell;'>$streak_type-$streak_length</div>";
  echo "<div class='cell' style='display:table-cell;'>$coaching_losses</div>";
  echo "<div class='cell' style='display:table-cell;'>$opponent_coaching_losses</div>";
  echo "</div>";  
}
echo "</div>";
ui_footer();
?>
<style>
* {
font-family:'Roboto', sans-serif;
}

paper-material {
    display: inline-block;
    background-color: #FFFFFF;
    padding: 32px;
    margin: 32px;
}

#standings-table {
  display: table;
  border-collapse: separate;
  font-size: 1vw;
  text-align: center;
}

#standings-table .cell {
  border-top: 1px solid #e5e5e5;
  padding: 16px;
}

#standings-table .thickline .cell {
  border-bottom: 5px solid #000000;
}

#standings-table .header .cell {
    font-weight: bold;
    font-size: 110%;
    padding-top: 0;
    border-top: 0;
}

.samdwich {
    background-color: #00FF00;
}
 
.sworls {
    color: #FFFFFF;
    background-color: #0000FF;
}
    
.jhka3 {
    color: #FFFFFF;
    background-color: #FF0000;
}
    
.murphmanjr {
    color: #FFFFFF;
    background-color: #6495ED;
}
    
.lukabear {
    color: #FFFFFF;
    background-color: #9B30FF;
}

.kvk {
    background-color: #FFAA00;
}

.palc {
    color: #FFFFFF;
    background-color: #900000;
}
</style>
