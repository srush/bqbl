<?php
require_once "lib/lib.php";
$week = min(17, isset($_GET['week']) ? pg_escape_string($_GET['week']) : upcomingWeek());

ui_header($title="Set BQBL Lineup");

if(isset($_GET['team'])) {
    $bqblTeam = bqblTeamStrToInt(pg_escape_string($_GET['team']));
} elseif(isset($_GET['teamnum'])) {
    $bqblTeam = pg_escape_string($_GET['teamnum']);
    } elseif(isset($_SESSION['user'])) {
    $bqblTeam = getBqblTeam($_SESSION['user']);
    if(!isset($_SESSION['bqbl_team'])) {
        $_SESSION['bqbl_team'] = $bqblTeam;
    }
} else {
    echo "Please set the 'team' parameter!";
    exit(0);
}

if(isset($_POST['submit'])) {
    $insertstarter1 = pg_escape_string($bqbldbconn, $_POST['starter1']);
    $insertstarter2 = pg_escape_string($bqbldbconn, $_POST['starter2']);
    if(($week < currentWeek()) || (time() > weekCutoffTime($week))) {
        echo "Error: lineups cannot be set after 5:30PST on Thursday";
    } elseif($insertstarter1 == $insertstarter2) {
        echo "Error: Cannot start the same team twice!";
    } else {
        $query = "SELECT * FROM lineup WHERE league='$league' AND bqbl_team='$bqblTeam' AND year='$year' AND week='$week';";
        if(pg_num_rows(pg_query($bqbldbconn, $query)) > 0) {
            $query = "UPDATE lineup SET starter1='$insertstarter1', starter2='$insertstarter2'
                  WHERE league='$league' AND bqbl_team='$bqblTeam' AND year='$year' AND week='$week';";
        } else {
            $query = "INSERT INTO lineup (year, week, league, bqbl_team, starter1, starter2)
                  VALUES ('$year', '$week', '$league', '$bqblTeam', '$insertstarter1', '$insertstarter2');";
        }
        pg_query($bqbldbconn, $query);
        echo "Updated lineup.";
    }
}


$allowediting = (($_SESSION['bqbl_team'] == $bqblTeam) || (isTed($bqblTeam) && isTed($_SESSION['bqbl_team']))) && ($week >= currentWeek());
$starts = getStarts($year, $week, $bqblTeam, $league);

$starter1 = $starter2 = "";
$query = "SELECT starter1, starter2 FROM lineup
          WHERE year='$year' AND week='$week' AND league='$league' AND bqbl_team='$bqblTeam';";
$result = pg_query($bqbldbconn, $query);
if(pg_num_rows($result) > 0) {
    list($starter1, $starter2) = pg_fetch_array($result);
}

echo "<div>";
if($allowediting) echo "<form method='post' action='$_SERVER[PHP_SELF]?week=$week'>";
echo "<table border=1 style='border-collapse: collapse;'>";
echo "<tr><th>Team</th><th>Starts</th><th>Wk$week Opponent<th>Wk$week Starter 1</th><th>Wk$week Starter 2</th></tr>";

$query = "SELECT nfl_team FROM roster
          WHERE league='$league' AND year='$year' AND bqbl_team='$bqblTeam';";
$result = pg_query($bqbldbconn, $query);
foreach (getRosters($year, $league, $week > $REG_SEASON_END_WEEK && $week != $PRO_BOWL_WEEK)[$bqblTeam] as $nflTeam) {
   $nflTeam = getPrimaryTeamCode($nflTeam);
   $team_list = getTeamCodesSQL($nflTeam);
   $query = "SELECT home_team, away_team FROM game 
    WHERE (home_team IN $team_list OR away_team IN $team_list) AND season_year='$year' AND week='$week' AND season_type='Regular';";
    $result = pg_query($nfldbconn, $query);
    if(pg_num_rows($result) == 0) {
      $opponent = "BYE";
    } else {
      list($home, $away) = pg_fetch_array($result);
      $isHome = in_array($home, getTeamCodes($nflTeam));
      if ($isHome) {
          $opponent = $away;
          $opponentHome = "";
          $nflTeamHome = "*";
      } else {
          $opponent = $home;
          $opponentHome = "*";
          $nflTeamHome = "";
      }
      $opponent = $isHome ? $away : $home;
    }
    $disabled = !$allowediting ? "disabled" : "";
    $selected1 = $nflTeam == $starter1 ? "checked" : "";
    $selected2 = $nflTeam == $starter2 ? "checked" : "";
    echo "<tr><td><a href='$sitepath/nfl.php?team=$nflTeam&year=$year'>$nflTeam</a>$nflTeamHome</td>
          <td>$starts[$nflTeam]</td>
          <td><a href='$sitepath/nfl.php?team=$opponent&year=$year'>$opponent</a>$opponentHome</td>
          <td align='center'><input type='radio' name='starter1' value='$nflTeam' $disabled $selected1></td>
          <td align='center'><input type='radio' name='starter2' value='$nflTeam' $disabled $selected2></td>
          </tr>\n";
}
echo "</table>\n";
echo "<span style='font-size:x-small;'>* - Home team</span><br>\n";
echo "</div>";
if($allowediting) echo "<input type='submit' name='submit' value='Save Lineup' style='font-size:40px;'>";
elseif(!isset($_SESSION['user'])) echo "<a href='$sitepath/auth/login.php'>Log in to edit lineup</a>";
echo "</form>";
$prevWeek = $week - 1;
$nextWeek = $week + 1;
if ($prevWeek >= 1) {
  echo "\n<br><a href='$sitepath/lineup.php?week=$prevWeek'>Previous Week</a>";
}
if ($nextWeek <= 17) {
  echo "\n<br><a href='$sitepath/lineup.php?week=$nextWeek'>Next Week</a>";
}

ui_footer();

function getStarts($year, $week, $bqblTeam, $league) {
    global $bqbldbconn;
    $starts = array();
    $result = pg_query($bqbldbconn, "SELECT starter1, starter2 FROM lineup 
        WHERE year='$year' AND week <= $week AND league='$league' AND bqbl_team='$bqblTeam';");
    while(list($starter1, $starter2) = pg_fetch_array($result)) {
        $starts[$starter1] = isset($starts[$starter1]) ? $starts[$starter1] + 1 : 1;
        $starts[$starter2] = isset($starts[$starter2]) ? $starts[$starter2] + 1 : 1;
    }
    return $starts;
}
?>
