import psycopg

_DATABASE = None


def _get_database():
  connection = psycopg.connect(
      "host=localhost user=bqbl password=nolongerjamamama dbname=nfldb",
      autocommit=True)
  return connection


def get_database():
  global _DATABASE
  if not _DATABASE or _DATABASE.closed:
    _DATABASE = _get_database()
  return _DATABASE


def insert_clause_for_ordered_dict(table, d):
  keys_str = ", ".join(d.keys())
  values_str = ", ".join("%s" for k in d.keys())
  query = "INSERT INTO %s (%s) VALUES (%s)" % (table, keys_str, values_str)
  return query

  # values_str = ", ".join(["%%(%s)s" % k for k in d.keys()])