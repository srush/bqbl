import pdb
import dateutil.relativedelta
import dateutil.parser
import datetime
import json
import sql
import urllib.request as request
from collections import OrderedDict
import pytz
import re

PLAY_TYPES = {
    '2': 'End Period',
    '3': 'Pass Incompletion',
    '5': 'Rush',
    '7': 'Sack',
    '8': 'Penalty',
    '9': 'Fumble Recovery (Own)',
    '12': 'Kickoff Return (Offense)',
    '17': 'Blocked Punt',
    '18': 'Blocked Field Goal',
    '20': 'Safety',
    '21': 'Timeout',
    '24': 'Pass Reception',
    '26': 'Pass Interception Return',
    '29': 'Fumble Recovery (Opponent)',
    '32': 'Kickoff Return Touchdown',
    '36': 'Interception Return Touchdown',
    '38': 'Blocked Field Goal Touchdown',
    '39': 'Fumble Return Touchdown',
    '51': 'Pass',
    '52': 'Punt',
    '53': 'Kickoff',
    '59': 'Field Goal Good',
    '60': 'Field Goal Missed',
    '65': 'End of Half',
    '66': 'End of Game',
    '67': 'Passing Touchdown',
    '68': 'Rushing Touchdown',
    '70': 'Coin Toss',
    '74': 'Official Timeout',
    '75': 'Two-minute warning',
    '79': 'End of Regulation'
}

PERIOD_TO_GAME_PHASE = {
    0: "Q1",
    1: "Q1",
    2: "Q2",
    3: "Q3",
    4: "Q4",
    5: "OT",
    6: "OT",
    7: "OT"
}


def _v2_non_core_url_():
  return "https://site.api.espn.com/apis/site/v2/sports/football/nfl"


def _v2_core_url_():
  return "http://sports.core.api.espn.com/v2/sports/football/leagues/nfl"


def _v3_api_url_():
  return "https://sports.core.api.espn.com/v3/sports/football/nfl"


def _nfl_to_espn_season_type_(type):
  if type == "Preseason":
    return 1
  elif type == "Postseason":
    return 3
  else:
    return 2


def _espn_to_nfl_season_type_(type):
  if type == 1:
    return "Preseason"
  elif type == 2:
    return "Regular"
  else:
    return "Postseason"


def _current_nfl_year_():
  """NFL Year starts March 1st"""
  return (datetime.datetime.now() -
          dateutil.relativedelta.relativedelta(months=2)).strftime("%Y")


def update_game(game_id):
  try:
    db = sql.get_database()
    db.execute("SELECT COUNT(*) FROM play")

    url = _v2_core_url_() + "/events/%s/competitions/%s/plays?limit=1000" % (
        game_id, game_id)
    print(url)
    with request.urlopen(url) as response:
      page = response.read()
      data = json.loads(page)
      total_plays = data["count"]
      play_count = len(data["items"])
      for i in range(play_count):
        insert_play(game_id, data["items"][i],
                    data["items"][i + 1] if i < play_count - 1 else None)
  except Exception as e:
    pdb.set_trace()
    raise e


def insert_play(game_id, play, next_play):
  print("POS TEAM: ", _extract_pos_team(play))
  pos_team = _espn_team_to_nfldb_team(_extract_pos_team(play))
  if pos_team == "0":
    return
  drive_id = _extract_drive(game_id, play)[len(game_id):]
  play_type = PLAY_TYPES[play["type"]["id"]]
  print("drive id", drive_id)
  if play["type"]["id"] not in PLAY_TYPES:
    PLAY_TYPES[play["type"]["id"]] = play["type"]
  # pdb.set_trace()
  play_sql = {}
  game_phase = PERIOD_TO_GAME_PHASE[play["period"]["number"]]
  if play_type == "End of Game":
    game_phase = "Final"
  if play_type == "End of Half":
    game_phase = "Half"
  if "REVERSED" in play["text"]:
    if not next_play:
      return
      # Sometimes the 'end' is wrong for reversed plays, so wait for next play
    if play["start"].get("yardLine", "start") == next_play["start"].get(
        "yardLine", "end") and play["start"].get(
            "down", "start") == next_play["start"].get("down", "end"):
      return
  same_team_at_end = play["start"].get("team", "start") == play["end"].get(
      "team", "end")
  play_sql["drive_id"] = drive_id
  play_sql["gsis_id"] = game_id
  play_sql["play_id"] = play["id"][len(game_id):]
  play_sql["pos_team"] = pos_team
  play_sql["yardline"] = "(%s)" % max(min(50, play["start"]["yardLine"] - 50),
                                      -50)
  play_sql["down"] = 1 if play["start"]["down"] == 0 else play["start"]["down"]
  play_sql["yards_to_go"] = play["start"]["distance"]
  play_sql["description"] = play["text"]
  play_sql["time"] = "(%s, %s)" % (game_phase, int(play["clock"]["value"]))
  play_sql["time_inserted"] = "NOW()"
  play_sql["time_updated"] = "NOW()"
  play_sql["first_down"] = int(play["end"]["down"] == 1 and same_team_at_end)
  play_sql["fourth_down_att"] = int(play["start"]["down"] == 4)
  play_sql["fourth_down_conv"] = int(play["start"]["down"] == 4 and
                                     play["end"]["down"] != 4 and
                                     same_team_at_end)
  play_sql["fourth_down_failed"] = int(play_sql["fourth_down_att"] and
                                       not play_sql["fourth_down_conv"])
  play_sql["third_down_att"] = int(play["start"]["down"] == 3)
  play_sql["third_down_conv"] = int(play["start"]["down"] == 3 and
                                    play["end"]["down"] < 3 and
                                    same_team_at_end)
  play_sql["third_down_failed"] = int(play_sql["third_down_att"] and
                                      not play_sql["third_down_conv"])
  play_sql["passing_first_down"] = int(
      play_sql["first_down"] and play["type"].get("abbreviation", "") == "REC")
  play_sql["penalty"] = int(play["type"].get("abbreviation", "") == "PEN")
  play_sql["penalty_first_down"] = int(play_sql["penalty"] and
                                       play_sql["first_down"])
  play_sql["penalty_yds"] = play["statYardage"] if play_sql["penalty"] else 0
  play_sql["rushing_first_down"] = int(
      play_sql["first_down"] and play["type"].get("abbreviation", "") == "RUSH")
  play_sql["timeout"] = int(play["type"].get("abbreviation", "") == "TO")
  play_sql["xp_aborted"] = int(False)
  players = {}
  for participant in play.get("participants", []):
    athlete = participant["athlete"]["$ref"]
    if athlete not in players:
      players[athlete] = set()
    players[athlete].add(participant["type"])
  for player, participantTypes in players.items():
    insert_play_player(game_id, drive_id, play, player, participantTypes)
  print(
      sql.insert_clause_for_ordered_dict("play", play_sql) %
      tuple(play_sql.values()))
  sql.get_database().execute(
      sql.insert_clause_for_ordered_dict("play", play_sql),
      list(play_sql.values()))
  sql.get_database().commit()


def insert_play_player(game_id, drive_id, play, player, participantTypes):
  try:
    play_type = PLAY_TYPES[play["type"]["id"]]
    all_participant_types = [p["type"] for p in play.get("participants", [])]
    yards = play["statYardage"] if "statYardage" in play else play["end"][
        "yardLine"] - play["start"]["yardLine"]
    player_sql = {}
    player_sql["drive_id"] = drive_id
    player_sql["gsis_id"] = game_id
    player_sql["play_id"] = play["id"][len(game_id):]
    player_sql["player_id"] = _extract_player(player)
    if player_sql["play_id"] == "337" and player_sql["player_id"] == "3918298":
      pdb.set_trace()

    same_team_at_end = play["start"].get("team", "start") == play["end"].get(
        "team", "end")
    participant = next(
        filter(lambda p: p.get("athlete", {}).get("$ref", "") == player,
               play["participants"]), None)
    if participant:
      espn_team_id = _extract_team_from_stats(
          participant.get("statistics", {}).get("$ref", ""))
      if espn_team_id:
        result = sql.get_database().execute(
            "SELECT team_id FROM team WHERE espn_id='%s'" % espn_team_id)
    else:
      result = sql.get_database().execute(
          "SELECT team FROM player WHERE player_id='%s'" %
          player_sql["player_id"])
    if result.rowcount:
      team_id, = result.fetchone()
    else:
      team_id = "0"  # TODO
    penalties = _get_enforced_penalties(play["text"])
    for penalty in penalties:
      yards += (1 if penalty["team"] == team_id else -1) * penalty["yards"]
      if "Intentional Grounding" in penalty[
          "penalty"] and "passer" in participantTypes:
        player_sql["passing_att"] = 1
        # TODO
        # player_sql["passing_yds"] = yards
    player_sql["team"] = team_id
    if "No Play." not in play["text"]:
      if "fumbler" in participantTypes:
        if "Aborted" not in play["text"]:
          player_sql["fumbles_tot"] = 1
          if (same_team_at_end and
              play_type in ["Punt", "Kickoff Return (Offense)"]) or (
                  not same_team_at_end and
                  not play_type in ["Punt", "Kickoff Return (Offense)"]):
            player_sql["fumbles_lost"] = 1
        else:
          player_sql["rushing_att"] = 1
      if "rusher" in participantTypes and "passer" not in participantTypes:
        player_sql["rushing_att"] = 1
        player_sql["rushing_yds"] = yards
        if play["type"]["id"] == "68":
          player_sql["rushing_tds"] = 1
        if play["type"]["id"] == "TODO":
          player_sql["rushing_twoptm"] = 1
      if "receiver" in participantTypes:
        player_sql["receiving_tar"] = 1
        if play_type == "Pass Reception":
          player_sql["receiving_yds"] = yards
          player_sql["receiving_rec"] = 1
        elif play_type == "Passing Touchdown":
          player_sql["receiving_yds"] = yards
          player_sql["receiving_rec"] = 1
          player_sql["receiving_tds"] = 1
        elif play_type == "Pass":
          player_sql["receiving_tar"] = 1
          if "incomplete" not in play["text"]:
            player_sql["receiving_yds"] = yards
            player_sql["receiving_rec"] = 1

        if "fumbler" in participantTypes:
          player_sql["receiving_yds"] = yards
          player_sql["receiving_rec"] = 1

      if "passer" in participantTypes:
        if "receiver" in all_participant_types:
          player_sql["passing_att"] = 1
          if "tackler" in all_participant_types:
            player_sql["passing_cmp"] = 1

        if play_type in ["Pass Reception", "Fumble Recovery (Own)"]:
          player_sql["passing_att"] = 1
          player_sql["passing_cmp"] = 1
          player_sql["passing_yds"] = yards
        elif play_type == "Pass Incompletion":
          player_sql["passing_att"] = 1
          player_sql["passing_cmp"] = 0
        elif play_type == "Pass":
          # Maybe a sack fumble, then a self recovery, then a pass
          player_sql["passing_att"] = 1
          player_sql["passing_cmp"] = int(not "incomplete" in play["text"])
          player_sql["passing_yds"] = yards
        elif play_type == "Fumble Recovery (Opponent)":
          result = re.search(r"Pass for (\d+) Yds", play["shortText"])
          if result:
            player_sql["passing_yds"] = result.groups(1)
          return None
        elif play_type == "Passing Touchdown":
          player_sql["passing_att"] = 1
          player_sql["passing_cmp"] = 1
          player_sql["passing_yds"] = yards
          player_sql["passing_tds"] = 1
        elif play_type == "Sack":
          player_sql["passing_sk_yds"] = _extract_sack_yds(play["shortText"])
          player_sql["passing_sk"] = 1
        elif play_type == "Safety":
          if "sackedBy" in all_participant_types:
            player_sql["passing_sk"] = 1
            player_sql["passing_sk_yds"] = yards
        elif play_type in [
            "Pass Interception Return", "Interception Return Touchdown"
        ]:
          player_sql["passing_att"] = 1
          player_sql["passing_cmp"] = 0
          player_sql["passing_int"] = 1
        else:
          raise Exception("Unhandled play type for passer: %s" % play_type)
      if "returner" in participantTypes:
        if play_type == "Pass Interception Return":
          player_sql["defense_int"] = 1
        elif play_type == "Interception Return Touchdown":
          player_sql["defense_int"] = 1
          player_sql["defense_int_tds"] = 1
        elif play_type == "Fumble Recovery (Opponent)":
          player_sql["defense_frec"] = 1
        elif play_type == "Fumble Return Touchdown":
          player_sql["defense_frec"] = 1
          player_sql["defense_frec_tds"] = 1

      if "passerPat" in participantTypes:
        if "ATTEMPT SUCCEEDS" in play["text"]:
          player_sql["passing_twoptm"] = 1
      if "recieverPat" in participantTypes:
        if "ATTEMPT SUCCEEDS" in play["text"]:
          player_sql["receiving_twoptm"] = 1
      if "rusherPat" in participantTypes:
        if "ATTEMPT SUCCEEDS" in play["text"]:
          player_sql["rushing_twoptm"] = 1
      if "scorer" in participantTypes:
        if play_type == "Safety":
          player_sql["defense_safe"] = 1
    print(player_sql)
    print(
        sql.insert_clause_for_ordered_dict("play_player", player_sql) %
        tuple(player_sql.values()))
    sql.get_database().execute(
        sql.insert_clause_for_ordered_dict("play_player", player_sql),
        list(player_sql.values()))

  except Exception as e:
    pdb.set_trace()
    raise e


def _get_enforced_penalties(text):
  result = re.findall(
      r"PENALTY on (\w+)-([^,]+), ([^,]*), (\d+) yards, enforced", text)
  return [{
      "team": r[0],
      "player_name": r[1],
      "penalty": r[2],
      "yards": int(r[3])
  } for r in result]


def _extract_sack_yds(shortText):
  result = re.search(r"Sacked by.* (\d+) Yd Loss", shortText)
  if result:
    print("SACK YARDS: ", result.group(1))
    return -1 * int(result.group(1))
  return 0  # TODO


def _extract_team_from_stats(stats_url):
  result = re.search(r"competitors/(\d+)/roster.*", stats_url)
  if result:
    return result.group(1)
  return None


def _extract_pos_team(play):
  result = re.search(r".*/teams/(\d+).*", play.get('team', {}).get('$ref', ""))
  if result:
    return result.group(1)
  return "0"  # TODO


def _extract_drive(game_id, play):
  result = re.search(r".*/drives/(\d+).*",
                     play.get('drive', {}).get('$ref', ""))
  if result:
    return result.group(1)
  return "%s00" % game_id


def _extract_player(player):
  result = re.search(r".*/athletes/(\d+).*", player)
  if result:
    return result.group(1)
  return 0


def _espn_team_to_nfldb_team(espn_team):
  print("espn team", espn_team)
  result = sql.get_database().execute(
      "SELECT team_id FROM team WHERE espn_id='%s';" % espn_team)
  if result.rowcount:
    team_id, = result.fetchone()
  else:
    team_id = "0"  # TODO
  return team_id


def update_schedules(year=_current_nfl_year_()):
  db = sql.get_database()
  cursor = db.execute(
      "SELECT team_id, espn_id FROM team WHERE espn_id IS NOT NULL;")
  for team_id, espn_id in cursor:
    insert_new_games(espn_id, year=year)


def insert_new_games(team_espn_id,
                     year=_current_nfl_year_(),
                     season_type=_nfl_to_espn_season_type_("REGULAR")):
  url = _v2_non_core_url_() + "/teams/%s/schedule?season=%s&seasontypes=%s" % (
      team_espn_id, year, season_type)
  print(url)
  db = sql.get_database()
  cursor = db.cursor()
  with request.urlopen(url) as response:
    page = response.read()
    data = json.loads(page)
    games = data["events"]
    current_time = datetime.datetime.now(
        datetime.timezone.utc)  #.strftime("%Y-%m-%d %H:%M:%S%z")
    for game in games:
      start_time = dateutil.parser.isoparse(game["date"])
      game_info = {}
      for competitor in game["competitions"][0]["competitors"]:
        if competitor["homeAway"] == "home":
          game_info["home_team"] = competitor["team"]["abbreviation"]
        else:
          game_info["away_team"] = competitor["team"]["abbreviation"]
      game_info["gsis_id"] = game["id"]
      game_info["start_time"] = start_time  #.strftime("%Y-%m-%d %H:%M:%S%z")
      game_info["day_of_week"] = start_time.strftime("%A")
      game_info["week"] = game["week"]["number"]
      game_info["season_year"] = year
      game_info["finished"] = False
      game_info["season_type"] = _espn_to_nfl_season_type_(season_type)
      game_info["time_inserted"] = current_time
      game_info["time_updated"] = current_time
      game_info["home_score"] = 0
      game_info["away_score"] = 0
      game_info["home_turnovers"] = 0
      game_info["away_turnovers"] = 0
      query = sql.insert_clause_for_ordered_dict(
          "game", game_info) + " ON CONFLICT (gsis_id) DO NOTHING"

      # populated_query = query % tuple(game_info.values())
      # print("executing: \"%s\"" % populated_query)
      cursor.execute(query, list(game_info.values()))
    db.commit()


def update_rosters():
  db = sql.get_database()
  positions = _get_positions()
  cursor = db.execute(
      "SELECT team_id, espn_id FROM team WHERE espn_id IS NOT NULL;")
  for team_id, espn_id in cursor:
    players = get_roster(espn_id)
    for player in players:
      position = player['position'].get("abbreviation", "")
      position = position if position in positions else "UNK"
      query = """
      INSERT INTO player (player_id, full_name, first_name, last_name, team, position, profile_id, status)
        VALUES(%s,%s, %s, %s, %s, %s, %s, 'Unknown') 
        ON CONFLICT (player_id) 
        DO 
        UPDATE SET team=%s, position=%s;
      """
      db.execute(query, (player['id'], player['fullName'], player['firstName'],
                         player['lastName'], team_id, position, player['id'],
                         team_id, position))
    db.execute(
        "UPDATE meta SET last_roster_download = now() at time zone 'utc'")
    db.commit()


def get_roster(espn_id):
  url = _v2_non_core_url_() + "/teams/%s/roster" % espn_id
  print(url)
  with request.urlopen(url) as response:
    page = response.read()
    data = json.loads(page)
    players = []
    for subteam in data["athletes"]:
      players += subteam["items"]
    return players


def get_all_players():
  url = _v3_api_url_() + "/athletes?limit=20000"
  with request.urlopen(url) as response:
    page = response.read()
    data = json.loads(page)
    players = data["items"]
    pdb.set_trace()


def update_team_ids():
  url = _v2_non_core_url_() + "/teams?limit=32"
  with request.urlopen(url) as response:
    page = response.read()
    data = json.loads(page)
    # print(data)
    teams = data['sports'][0]['leagues'][0]['teams']
    db = sql.get_database()
    for team in teams:
      query = "UPDATE team SET espn_id='%s' WHERE team_id='%s'" % (
          team['team']['id'], team['team'].get("abbreviation", ""))
      db.execute(query)
    db.commit()


def _get_positions():
  cursor = sql.get_database().execute(
      "SELECT unnest(enum_range(null::player_pos));")
  return [r[0] for r in cursor.fetchall()]
