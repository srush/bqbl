<?php
require_once "lib/lib.php";
require_once "lib/scoring.php";

$year = isset($_GET['year']) ? pg_escape_string($_GET['year']) : currentYear();
$week_complete = min(15, $year < currentYear() ? 15 : currentCompletedWeek());
$league = isset($_GET['league']) ? $_GET['league'] : getLeague();

$games = array();
foreach (nflTeams() as $team) {
    for ($i=1; $i<=$week_complete; $i++) {
        $games[] = array($year, $i, $team);
    }
}

$gamePoints = getPointsBatch($games);

ui_header("$year BQBL Schedule");

$bqbl_teamname = bqblTeams($league, $year);
$matchup = array();
$score = array();

$query = "SELECT week, team1, team2
            FROM schedule
              WHERE year='$year' AND league='$league' AND week <= '$REG_SEASON_END_WEEK';";
$result = pg_query($bqbldbconn, $query);
while(list($week,$team1,$team2) = pg_fetch_array($result)) {
    $matchup[$week][$team1] = $team2;
    $matchup[$week][$team2] = $team1;
}

$query = "SELECT bqbl_team, nfl_team
    FROM roster WHERE year='$year';";  
$result = pg_query($GLOBALS['bqbldbconn'],$query); 
while(list($bqbl_team,$nfl_team) = pg_fetch_array($result)) {
      $roster[$bqbl_team][] = $nfl_team;                         
}

echo '<paper-material elevation="2">';
echo '<div id="schedule-table">';
echo "<div class='header row'><div class='cell'></div>";
foreach($bqbl_teamname as $teamId => $teamName) {
    if ($teamName == "Anirbaijan") {
        echo "<div class='cell'><span class='rainbow'><a class='nolinkcolor' href='" . getBqblTeamLink($year, $league, $teamId) . "'>$teamName</a></span></div>";
    } else {
        echo "<div class='cell'><a class='nolinkcolor' href='" . getBqblTeamLink($year, $league, $teamId) . "'>$teamName</a></div>";
    }
}
echo "</div>";

for ($i = 1; $i <= 15; $i++) {
    if ($i == 15 && $year > 2013) {
        continue;
    }
    $lineup = getLineups($year, $i, $league);
    foreach ($roster as $bqbl_team => $nfl_teams) {
        $score[$bqbl_team][$i] = 0;
        if ($i > $week_complete) {
            continue;
        }

        foreach ($nfl_teams as $nfl_team) {
            if ($nfl_team == $lineup[$bqbl_team][0] || $nfl_team == $lineup[$bqbl_team][1]) {         
                $score[$bqbl_team][$i] += totalPoints($gamePoints[$year][$i][$nfl_team]);             
            } else {                                                                                  
                $score[$bqbl_team][$i] += $gamePoints[$year][$i][$nfl_team]['Misc. Points'][1];       
            }
        }                                                                                             
    }
   
    echo "<div class='row'><div class='cell'><a class='nolinkcolor' href='$sitepath/matchup.php?week=$i'>Week $i</a></div>";
    foreach ($bqbl_teamname as $teamId => $teamName) {
        if ($score[$teamId][$i] > $score[$matchup[$i][$teamId]][$i]) {
            echo "<div class='cell win'>".$bqbl_teamname[$matchup[$i][$teamId]]."</div>";
        } elseif ($score[$teamId][$i] < $score[$matchup[$i][$teamId]][$i]) {
            echo "<div class='cell loss'>".$bqbl_teamname[$matchup[$i][$teamId]]."</div>";
        } else {
            echo "<div class='cell'>".$bqbl_teamname[$matchup[$i][$teamId]]."</div>";
        }
    }
    echo "</div>";
}
echo "</div>";
ui_footer();
?>
<style is="custom-style">

paper-material {
    display: inline-block;
    background-color: #FFFFFF;
    padding: 32px;
    margin: 32px 32px 0 32px;
}

.loss {
    background-color: var(--paper-red-500);
}

.win {
    background-color: var(--paper-green-500);
}

.row {
    display: table-row;
}

.cell {
    display: table-cell;
}

#schedule-table {
  border-collapse: separate;
  font-size: .75vw;
  text-align: center;
}

#schedule-table .cell {
  border-top: 1px solid #e5e5e5;
  padding: 16px;
}

#schedule-table .thickline .cell {
  border-bottom: 5px solid #000000;
}

#schedule-table .header .cell {
    font-weight: bold;
    font-size: 110%;
    padding-top: 0;
    border-top: 0;
}
</style>

<?
footer();
exit();
die();
?>
