
<?php
require_once "lib/lib.php";
require_once "lib/scoring.php";

ui_header($title="BQBL Scoreboard - Week $week $year", $showLastUpdated=true, $showAutoRefresh=true, $showWeekDropdown=true);

$statDisplayNameMap = array();
$statDisplayNameMap["Completion Pct"] = "Comp %";
$statDisplayNameMap["Game Winning Drive"] = "GWD";
$statDisplayNameMap["Sacks and Groundings"] = "Sacks";
$statDisplayNameMap["Total Yards"] = "Yards";
$statDisplayNameMap["Misc. Points"] = "Misc";
$statDisplayNameMap["Turnovers"] = "TOs";
$statDisplayNameMap["Fumbles Kept"] = "Fumblets";


if ($week == $PRO_BOWL_WEEK) {
    $league = "probowl";
}

$records = array();
$prevWeek = min($week - 1, 14);
$query = "SELECT bqbl_team, wins, losses
    FROM standings($year, $prevWeek, '$league');";
$result = pg_query($GLOBALS['bqbldbconn'],$query);
while(list($bqbl_team, $wins, $losses) = pg_fetch_array($result)) {
  $records[$bqbl_team] = array($wins, $losses);
}

$bqbl_teamname = bqblTeams($league, $year);
$starters = getLineups($year, $week, $league);
$lineup = $starters;
foreach (getRosters($year, $league, $week > $REG_SEASON_END_WEEK && $week != $PRO_BOWL_WEEK) as $bqbl_team => $roster) {
    foreach ($roster as $nfl_team) {
        if (!$lineup[$bqbl_team] or !in_array($nfl_team, $lineup[$bqbl_team])) {
            $lineup[$bqbl_team][] = $nfl_team;
        }
    }
}
$unsortedmatchup = getMatchups($year, $week, $league);
$matchup=array();
foreach ($unsortedmatchup as $bqblteam1 => $bqblteam2) {
    if ($_SESSION['bqbl_team']==$bqblteam1 || $_SESSION['bqbl_team']==$bqblteam2) {
        $matchup[$bqblteam1] = $bqblteam2;
    }
}
foreach ($unsortedmatchup as $bqblteam1 => $bqblteam2) {
    if ($_SESSION['bqbl_team']!=$bqblteam1 && $_SESSION['bqbl_team']!=$bqblteam2) {
        $matchup[$bqblteam1] = $bqblteam2;
    }
}

$games = array();
foreach(nflTeams() as $nflTeam) $games[] = array($year, $week, $nflTeam);
$gamePoints = getPointsBatch($games);

foreach ($matchup as $bqblteam1 => $bqblteam2) {
    $home_team1 = abbreviatedPoints($gamePoints[$year][$week][$lineup[$bqblteam1][0]]);
    $home_team2 = abbreviatedPoints($gamePoints[$year][$week][$lineup[$bqblteam1][1]]);
    $home_team3 = abbreviatedPoints($gamePoints[$year][$week][$lineup[$bqblteam1][2]]);
    $home_team4 = abbreviatedPoints($gamePoints[$year][$week][$lineup[$bqblteam1][3]]);
    $away_team1 = abbreviatedPoints($gamePoints[$year][$week][$lineup[$bqblteam2][0]]);
    $away_team2 = abbreviatedPoints($gamePoints[$year][$week][$lineup[$bqblteam2][1]]);
    $away_team3 = abbreviatedPoints($gamePoints[$year][$week][$lineup[$bqblteam2][2]]);
    $away_team4 = abbreviatedPoints($gamePoints[$year][$week][$lineup[$bqblteam2][3]]);
    $populatedTeam = $home_team1;
    if(count($home_team2)>0 && $home_team2["Interceptions"] != '') $populatedTeam = $home_team2;
    elseif(count($away_team1)>0 && $away_team1["Interceptions"] != '') $populatedTeam = $away_team1;
    elseif(count($away_team2)>0 && $away_team2["Interceptions"] != '') $populatedTeam = $away_team2;
    $columns = 2 + count($populatedTeam);
    $statcolumns = $columns - 1;

    echo "<paper-material elevation='5' class='matchuppaper x-scope paper-material-0'><div style=\"display:inline-table;font-family:'Roboto', sans-serif;font-size: 1.15vw;max-width:100%;\" class='matchup'>";
    $totalteam1 = totalTeamScore($gamePoints[$year][$week], $lineup[$bqblteam1], $starters[$bqblteam1]);
    $team1wins = $records[$bqblteam1][0];
    $team1losses = $records[$bqblteam1][1];
    echo "<paper-material elevation='1' class='teampaper x-scope paper-material-0'>
    <span class='teamname'>
      <span style='color:#999999;font-size:small;vertical-align:middle;'>($team1wins-$team1losses)</span>
      <a class='nolinkcolor' href='" . getBqblTeamLink($year, $league, $bqblteam1) . "'>$bqbl_teamname[$bqblteam1]</a>:&nbsp; $totalteam1
    </span>";
    echo "<div style='display:table-row;'>";
    echo "<div class='cell'></div>";
    echo "<div class='cell statheader'></div>";
    echo "<div class='cell totalpointscell'>Total</div>";
    foreach($populatedTeam as $name => $val) {
      $name = $statDisplayNameMap[$name] ? $statDisplayNameMap[$name] : $name;
      echo "<div class='cell statheader'>$name</div>";
    }
    echo "</div>";

    printTeamRow($lineup[$bqblteam1][0], $home_team1, in_array($lineup[$bqblteam1][0], $starters[$bqblteam1]));
    printTeamRow($lineup[$bqblteam1][1], $home_team2, in_array($lineup[$bqblteam1][1], $starters[$bqblteam1]));
    printTeamRow($lineup[$bqblteam1][2], $home_team3, in_array($lineup[$bqblteam1][2], $starters[$bqblteam1]));
    printTeamRow($lineup[$bqblteam1][3], $home_team4, in_array($lineup[$bqblteam1][3], $starters[$bqblteam1]));

    $totalteam2 = totalTeamScore($gamePoints[$year][$week], $lineup[$bqblteam2], $starters[$bqblteam2]);
    echo "</paper-material><br>";
    $team2wins = $records[$bqblteam2][0];
    $team2losses = $records[$bqblteam2][1];
    echo "<paper-material elevation='1' class='teampaper x-scope paper-material-0'>
    <span class='teamname'>
      <span style='color:#999999;font-size:small;vertical-align:middle;'>($team2wins-$team2losses)</span>
      <a class='nolinkcolor' href='" . getBqblTeamLink($year, $league, $bqblteam2) . "'>$bqbl_teamname[$bqblteam2]</a>:&nbsp; $totalteam2
    </span>";

    echo "<div style='display:table-row'>";
    echo "<div class='cell'></div>";
    echo "<div class='cell statheader'></div>";
    echo "<div class='cell totalpointscell'>Total</div>";
    foreach($populatedTeam as $name => $val) {
      $name = $statDisplayNameMap[$name] ? $statDisplayNameMap[$name] : $name;
      echo "<div class='cell statheader'>$name</div>";
    }
    echo "</div>";

    printTeamRow($lineup[$bqblteam2][0], $away_team1, in_array($lineup[$bqblteam2][0], $starters[$bqblteam2]));
    printTeamRow($lineup[$bqblteam2][1], $away_team2, in_array($lineup[$bqblteam2][1], $starters[$bqblteam2]));
    printTeamRow($lineup[$bqblteam2][2], $away_team3, in_array($lineup[$bqblteam2][2], $starters[$bqblteam2]));
    printTeamRow($lineup[$bqblteam2][3], $away_team4, in_array($lineup[$bqblteam2][3], $starters[$bqblteam2]));

    echo "</div></paper-material></paper-material>";
}

ui_footer();

function abbreviatedPoints($fullPoints) {
  $points["Completion Pct"] = $fullPoints["Completion Pct"];
  $points["Total Yards"] = $fullPoints["Total Yards"];
  $points["Longest Play"] = $fullPoints["Longest Play"];
  $points["Long Plays"] = $fullPoints["Long Plays"];
  $points["TDs"] = $fullPoints["TDs"];
  $points["Sacks and Groundings"] = $fullPoints["Sacks and Groundings"];  
  if ($fullPoints["Turnovers"]) { // Check if any stats exist
    $points["TOs"] = array(
      $fullPoints["Turnovers"][0],
      $fullPoints["TOs"][1] + $fullPoints["TD TOs"][1] + $fullPoints["Turnovers"][1] + (($fullPoints["OT TOs"][1] + $fullPoints["OT TD TOs"][1]) / 2)
    );
  } else {
    $points["TOs"] = null;
  }
  $points["Fumbles Kept"] = $fullPoints["Fumbles Kept"];    
  if ($fullPoints["Safeties"]) { // Check if any stats exist
    $points["Safeties"] = array(
      $fullPoints["Safeties"][0] + $fullPoints["OT Safeties"][0],
      $fullPoints["Safeties"][1] + ($fullPoints["OT Safeties"][1] / 2)
    );
  } else {
    $points["Safeties"] = null;
  }
  $points["Benchings"] = $fullPoints["Benchings"];
  $points["Game Winning Drive"] = $fullPoints["Game Winning Drive"];
  if ($fullPoints["OT TOs"]) { // Check if any stats exist
    $points["OT Bonus"] = array(
      0,
      ($fullPoints["OT TOs"][1] + $fullPoints["OT TD TOs"][1] + $fullPoints["OT Safeties"][1]) / 2
    );
  } else {
    $points["OT Bonus"] = null;
  }
  $points["Misc. Points"] = $fullPoints["Misc. Points"];

  
  unset($points["TD TOs"]);
  unset($points["Turnovers"]);
  unset($points["OT TOs"]);
  unset($points["OT TD TOs"]);
  unset($points["OT Safeties"]);
  return $points;
}

function cmp_isTeamUser($a, $b) {
    if (isset($_SESSION['bqbl_team'])) {
        if ($a == $_SESSION['bqbl_team']) {
            return -1;
        } elseif ($b == $_SESSION['bqbl_team']) {
            return 1;
        }
    }
    return 0;
}


function printTeamRow($team, $points, $starting=false) {
    global $statcolumns, $year, $week;
    $style = $starting ? "background: #00CC66;" : "";
    echo "<div style='display:table-row;'>";
    $gsis_id = gsisId($year, $week, $team);
    $gameTypeAndTime = gameTypeAndTime($gsis_id);
    $timeClass = 'gameTimeOver';
    if ($gameTypeAndTime[0] == 1) $timeClass = 'gameTimeActive';
    elseif ($gameTypeAndTime[0] == 2 || ($gameTypeAndTime[0] == -1 && !isPast($year, $week))) $timeClass = 'gameTimeInactive';
    $teamNameClass = $gameTypeAndTime[0] == 1 ? 'nflTeamNameActive' : 'nflTeamNameInactive';
    if ($gameTypeAndTime[0] != -1) {
      $gameUrl = gameUrl($gsis_id);
      echo "<div class='cell $timeClass'><a class='nolinkcolor' href='$gameUrl'>$gameTypeAndTime[1]</a></div>\n";
    } else {
      echo "<div class='cell $timeClass'>$gameTypeAndTime[1]</div>\n";
    }
    echo "<div class='cell' style='$style'>
        <a class='nolinkcolor' href='/bqbl/nfl.php?team=$team&year=$year'>$team</a></div>";
    if ($starting) {
      echo "<div class='cell totalpointscell'><span class='totalpoints'>" . totalPoints($points) . "</span></div>";
    } else {
      echo "<div class='cell totalpointscell'><span class='totalpoints'>" . totalPoints(getPointsOnlyMisc($points)) . "</span><span class='inactivestatpoints'>(" . totalPoints($points) . ")</span></div>";
    }
    $statpoints = $starting ? $points : getPointsOnlyMisc($points);
    foreach($statpoints as $name => $val) {
        if ($val == '') {
            echo "<div class='cell'></div>";
        } else {
            echo "<div class='cell'><span class='statpoints'>$val[1]</span><span class='statvalue'>";              
            if ($name != "Misc. Points" && $name != "OT Bonus") {
                echo "($val[0])";
            }
            echo "</div>";
        }
    }
    echo "</div>\n";
}

function tableCells($cells) {
    for ($i = 0; $i < $cells; $i++) {
        echo "<div style='width:0px;'></div>";
    }
}
?>
