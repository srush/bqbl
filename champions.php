<?php
require_once "lib/lib.php";
ui_header($title="Past Champions");
?>
<style is="custom-style">
#content {
	margin: 32px;
	text-align: left;
}
paper-material {
    display: block;
    background-color: #FFFFFF;
    padding: 32px;
    margin: 32px 24px 0 24px;
}
</style>
<paper-material elevation="1" class="x-scope paper-material-0">
    
    <h2>2016 BQBL Champion</h2>
    <h3><font color="#9B30FF">Lukabear</font></h3>
    <img src="media/luka_front.jpg" alt="Luka Front" style="width:304px;height:228px">
    <img src="media/luka_back.jpg" alt="Luka Back" style="width:304px;height:228px">
<br>In perhaps the most impressive display in league history, Lukabear won this championship without losing a single game. He exacted revenge on the much disputed 2015 champion, Colinlusi'n, who did not manage to win a single game in the 2016 season.
  </paper-material>
<paper-material elevation="1" class="x-scope paper-material-0">
    
    <h2>2015 BQBL Champion</h2>
    <h3><font color="6495ED">Colinlusi'n*</font></h3>
		<font size="-4" color="red">* This championship was heavily disputed by Luka Mernik and many people are saying it was illegitimate.</font>

  </paper-material>
<paper-material>
    <h2>2014 BQBL Champion</h2>
    <h3><font color='red'>James Hans Kristian Anderson III</font></h3>
</paper-material>
<paper-material>
    <h2>2013 BQBL Champion</h2>
    <h3><font color='orange'>Keevon von Kevin</font></h3>
    <img src="media/kevin_front.jpg" alt="Kevin Front" style="width:172px;height:228px">
</paper-material>
<paper-material>
    <h2>2012 BQBL Champion</h2>
    <h3><font color='red'>James Hans Kristian Anderson III</font></h3>
    <img src="media/jim_front.jpg" alt="Jim Front" style="width:304px;height:228px">
    <img src="media/jim_back.jpg" alt="Jim Back" style="width:304px;height:228px">
</paper-material>
<?php
ui_footer();
?>
